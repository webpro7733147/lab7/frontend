import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
    const isLoadding = ref(false)
    const doLoad = () => {
        isLoadding.value = true
    }
    const finish = () => {
        isLoadding.value = false
    }
    return { isLoadding, doLoad, finish }
})
