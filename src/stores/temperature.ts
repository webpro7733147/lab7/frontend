import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/service/http'
import { useLoaddingStore } from './loadding'
import temperatureService from '@/service/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loaddingStore = useLoaddingStore()

  async function callConvert() {
    console.log('Store: Call Convert');
    
    loaddingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e);
    }
    loaddingStore.finish()
    console.log('Store: Finish Call Convert');
  }

  return { valid, result, celsius, callConvert }
})
