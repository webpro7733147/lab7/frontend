import http from "./http"
type ReturnData = {
    celsius: number
    fahrenheit: number
}
async function convert(celsius: number): Promise<number> {
    //query `http://localhost:3000/temperature/convert?celsius=${celsius.value}`
    //params `http://localhost:3000/temperature/convert/${celsius.value}`
    //body
    console.log('Service: Call Convert');

    const res = await http.post(`/temperature/convert`, {
        celsius: celsius
    })
    const convertResult = res.data as ReturnData
    return convertResult.fahrenheit
}
export default { convert }